select date,
       bidder_name,
       device_type,
       sum(bid_requests) as bid_requests,
       sum(bid_responses) as bid_responses,
       sum(bids_matched) as bids_matched,
       sum(bid_wins) as bid_wins,
       sum(bid_wins)/cast(sum(total_bid_wins) as double) as win_rate,
       sum(avg_bid_cpm*bids_matched)/cast(sum(bids_matched) as double) as avg_bid_cpm
from header_bidding
where date = date('')
      and site_key = 'site'
group by date,
         bidder_name,
         device_type
order by 1,2,3
