select date,
       site_key,
       device_type,
       sum(pageviews) as pageviews,
       sum(users) as users,
       sum(sessions) as sessions,
       sum(ad_inventory_name) as ad_inventory_name,
       sum(impressions) as impressions,
       sum(engaged_pageviews)/cast(sum(pageviews) as double) as engaged_percentage,
       sum(out_of_focus_pageviews)/cast(sum(pageviews) as double) as out_of_focus_percentage,
       sum(median_page_length*pageviews)/cast(sum(pageviews) as double) as avg_page_length,
       sum(avg_time_on_page*pageviews)/cast(sum(pageviews) as double) as avg_time_on_page
from pages_engagement
where date = date('')
      and site_key = 'site'
      and url = 'ALL' /*if you want to see data per url you can change this row to: url <> 'ALL'. You will not be able to see sessions/pageviews data with this dimension*/
group by date,
         site_key,
         device_type
order by 1,2,3
