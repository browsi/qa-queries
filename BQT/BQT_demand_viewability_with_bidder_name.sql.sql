select date,
       site_key,
       device_type,
       bidder_name,
       sum(impressions) as impressions,
       sum(viewed_impressions) as viewed_impressions,
       sum(viewed_impressions)/cast(sum(impressions) as double) as viewability,
       sum(viewed_impressions_100) as viewed_impressions_100,
       sum(viewed_impressions_100)/cast(sum(impressions) as double) as viewability_100,
       sum(time_in_view*viewed_impressions)/cast(sum(viewed_impressions) as double) as avg_time_in_view,
       sum(percentage_in_view*viewed_impressions)/cast(sum(viewed_impressions) as double) as avg_percentage_in_view
from demand_viewability
where date = date('')
      and site_key = 'site'
      and bidder_name <> 'ALL'
group by date,
         site_key,
         device_type,
         bidder_name
order by 1,2,3,4
