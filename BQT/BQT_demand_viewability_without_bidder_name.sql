select date,
       site_key,
       device_type,
       sum(ad_requests) as ad_requests,
       sum(impressions) as impressions,
       sum(impressions)/cast(sum(ad_requests) as double) as fill_rate,
       sum(viewed_impressions) as viewed_impressions,
       sum(viewed_impressions)/cast(sum(impressions) as double) as viewability,
       sum(viewed_impressions_100) as viewed_impressions_100,
       sum(viewed_impressions_100)/cast(sum(impressions) as double) as viewability_100,
       sum(avg_ad_location*impressions)/cast(sum(impressions) as double) as avg_ad_location,
       sum(time_in_view*viewed_impressions)/cast(sum(viewed_impressions) as double) as avg_time_in_view,
       sum(percentage_in_view*viewed_impressions)/cast(sum(viewed_impressions) as double) as avg_percentage_in_view
from demand_viewability
where date = date('')
      and site_key = 'site'
      and bidder_name = 'ALL'
group by date,
         site_key,
         device_type
order by 1,2,3
