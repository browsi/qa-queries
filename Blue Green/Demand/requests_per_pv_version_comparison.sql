select a.url,
       old_pvs,
       new_pvs,
       old_requests,
       new_requests,
       old_requests/cast(old_pvs as double) as old_requests_per_pv,
       new_requests/cast(new_pvs as double) as new_requests_per_pv,
       new_requests/cast(new_pvs as double)-old_requests/cast(old_pvs as double) as difference
from
(
select url_decode(url) as url,
       count (distinct case when middy_version = 'old_version' then page_view_id end) as old_pvs,
       count (distinct case when middy_version = 'new_version' then page_view_id end) as new_pvs
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and site_key = 'site'
      and middy_version in('old_version','new_version')
      and event_type = 'page_view'
      and device_type in('MOBILE','TABLET')
      and is_benchmark_pageview = false
group by 1
)a

left join

(
select url_decode(url) as url,
       count (distinct case when middy_version = 'old_version' then concat(page_view_id,cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as old_requests,
      count (distinct case when middy_version = 'new_version' then concat(page_view_id,cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as new_requests
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and site_key = 'site'
      and middy_version in('old_version','new_version')
      and event_type = 'ad_request'
      and device_type in('MOBILE','TABLET')
      and is_benchmark_pageview = false
group by 1
)b

on a.url = b.url

order by 3 desc
