select url_decode(url) as url,
       count(distinct case when event_type = 'bid_request' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as old_requests,
       count(distinct case when event_type = 'bid_matched' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as old_ads_matched,
       count(distinct case when event_type = 'bid_matched' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'bid_request' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) as old_percentage,
       count(distinct case when event_type = 'bid_request' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as new_requests,
       count(distinct case when event_type = 'bid_matched' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as new_ads_matched,
       count(distinct case when event_type = 'bid_matched' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'bid_request' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) as new_percentage,
         count(distinct case when event_type = 'bid_matched' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'bid_request' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) - count(distinct case when event_type = 'bid_matched' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'bid_request' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) as difference
from events_db.header_bidding
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and event_type in('bid_request','bid_matched')
      and site_key = 'site' 
      and middy_version in('old_version','new_version')
      /* optional
      and bidder_name = 'bidder'
      and demand_index = 0
      and attempt_index = 1*/
group by url_decode(url)
having count(distinct case when event_type = 'bid_request' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) >= 50 /*minimun 50 requests per URL, change it to whatever fits your data needs*/
order by 5 desc /*ordered by new bid requests descending*/


