select url_decode(url) as url,
       count (distinct case when middy_version = 'old_version' then page_view_id end) as old_pvs,
       count (distinct case when middy_version = 'new_version' then page_view_id end) as new_pvs,
       exp(avg(ln(case when middy_version = 'old_version' then time_offset end))) as old_avg_time_offset,
       exp(avg(ln(case when middy_version = 'new_version' then time_offset end))) as new_avg_time_offset,
       exp(avg(ln(case when middy_version = 'new_version' then time_offset end)))-exp(avg(ln(case when middy_version = 'old_version' then time_offset end))) as difference
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type = 'event_type' /*choose only one event type*/
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and site_key = 'site'
      and middy_version in('old_version','new_version')
group by url_decode(url)
order by 3 desc /*ordered by new pvs descending*/
