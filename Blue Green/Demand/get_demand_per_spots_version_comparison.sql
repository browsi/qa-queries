/*get demand and spots found have been normalized to max items (no potential spots)*/

select q1.site_key,
       url,
       sum (case when q1.middy_version = 'old_version' and spots_found > max_items then max_items 
                 when q1.middy_version = 'old_version' and spots_found <= max_items then spots_found end) as old_spots_found,
	   sum (case when q1.middy_version = 'new_version' and spots_found > max_items then max_items 
                 when q1.middy_version = 'new_version' and spots_found <= max_items then spots_found end) as new_spots_found,
       sum (case when q1.middy_version = 'old_version' and get_demand > max_items then max_items 
                 when q1.middy_version = 'old_version' and get_demand <= max_items then get_demand end) as old_get_demand,
	   sum (case when q1.middy_version = 'new_version' and get_demand > max_items then max_items 
                 when q1.middy_version = 'new_version' and get_demand <= max_items then get_demand end) as new_get_demand,
	   sum (case when q1.middy_version = 'old_version' and get_demand > max_items then max_items 
                 when q1.middy_version = 'old_version' and get_demand <= max_items then get_demand end)/cast(sum (case when q1.middy_version = 'old_version' and spots_found > max_items then max_items 
                 when q1.middy_version = 'old_version' and spots_found <= max_items then spots_found end) as double) as old_percentage,
	   sum (case when q1.middy_version = 'new_version' and get_demand > max_items then max_items 
                 when q1.middy_version = 'new_version' and get_demand <= max_items then get_demand end)/cast(sum (case when q1.middy_version = 'new_version' and spots_found > max_items then max_items 
                 when q1.middy_version = 'new_version' and spots_found <= max_items then spots_found end) as double) as new_percentage,
	   sum (case when q1.middy_version = 'new_version' and get_demand > max_items then max_items 
                 when q1.middy_version = 'new_version' and get_demand <= max_items then get_demand end)/cast(sum (case when q1.middy_version = 'new_version' and spots_found > max_items then max_items 
                 when q1.middy_version = 'new_version' and spots_found <= max_items then spots_found end) as double)-sum (case when q1.middy_version = 'old_version' and get_demand > max_items then max_items 
                 when q1.middy_version = 'old_version' and get_demand <= max_items then get_demand end)/cast(sum (case when q1.middy_version = 'old_version' and spots_found > max_items then max_items 
                 when q1.middy_version = 'old_version' and spots_found <= max_items then spots_found end) as double) as difference
from
(
select middy_version,
       site_key,
       url_decode(url) as url,
	   page_view_id,
       count(distinct case when event_type = 'spot_found' then concat(page_view_id, cast(ad_index as varchar)) end) as spots_found,
       count(distinct case when event_type = 'get_demand' then concat(page_view_id, cast(ad_index as varchar)) end) as get_demand
from events_db.demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET')
      and event_type in('spot_found','get_demand','ad_request')
      and site_key = 'site'
      and middy_version in('old_version','new_version')
group by 1,2,3,4
)q1

join

(
select middy_version,
       site_key,
       page_view_id,
       max_items
from events_db.supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET')
      and event_type = 'page_view'
      and site_key = 'site'
      and middy_version in('old_version','new_version')
)q2

on q1.middy_version = q2.middy_version
   and q1.site_key = q2.site_key
   and q1.page_view_id = q2.page_view_id

group by 1,2
order by 4 desc
