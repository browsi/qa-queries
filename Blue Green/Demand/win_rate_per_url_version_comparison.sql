select c.url,
       c.bidder_name,
       old_wins,
       new_wins,
       old_impressions,
       new_impressions,
       old_wins/cast(old_impressions as double) as old_win_rate,
       new_wins/cast(new_impressions as double) as new_win_rate,
       (new_wins/cast(new_impressions as double))-(old_wins/cast(old_impressions as double)) as difference
from
(
select a.url,
       a.bidder_name,
       count (distinct case when middy_version = 'old_version' then concat(a.page_view_id,cast(a.ad_index as varchar),cast(coalesce(a.refresh_count,0) as varchar)) end) as old_wins,
       count (distinct case when middy_version = 'new_version' then concat(a.page_view_id,cast(a.ad_index as varchar),cast(coalesce(a.refresh_count,0) as varchar)) end) as new_wins      
from
(
select middy_version,
       url_decode(url) as url,
       bidder_name,
       page_view_id,
       ad_index,
       refresh_count
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and event_type = 'bid_request'
      and site_key = 'site' 
      and middy_version in('old_version','new_version')
)a

join


(
select bidder_name,
       page_view_id,
       ad_index,
       refresh_count
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and event_type = 'hbw'
      and site_key = 'site' 
)b

on a.bidder_name = b.bidder_name
   and a.page_view_id = b.page_view_id
   and a.ad_index = b.ad_index
   and a.refresh_count = b.refresh_count

group by a.url,
         a.bidder_name
)c

left join


(
select url_decode(url) as url,
       count(distinct case when middy_version = 'old_version' then concat(page_view_id,cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as old_impressions,
       count(distinct case when middy_version = 'new_version' then concat(page_view_id,cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as new_impressions
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and event_type = 'ad_impression'
      and provider = 'header_bidding'
      and site_key = 'site' 
      and middy_version in('old_version','new_version')
group by url_decode(url)
)d

on c.url = d.url

order by old_wins desc
