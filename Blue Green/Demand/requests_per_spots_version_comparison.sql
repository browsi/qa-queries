/*This query only works for desktop OR sites with NO potential spots!!!!*/
select url_decode(url) as url,
       count(distinct case when event_type = 'spot_found' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar)) end) as old_spots,
       count(distinct case when event_type = 'ad_request' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar)) end) as old_requests,
       count(distinct case when event_type = 'ad_request' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar)) end)/cast(count(distinct case when event_type = 'spot_found' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar)) end) as double) as old_percentage,
       count(distinct case when event_type = 'spot_found' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar)) end) as new_spots,
       count(distinct case when event_type = 'ad_request' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar)) end) as new_requests,
       count(distinct case when event_type = 'ad_request' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar)) end)/cast(count(distinct case when event_type = 'spot_found' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar)) end) as double) as new_percentage,
         count(distinct case when event_type = 'ad_request' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar)) end)/cast(count(distinct case when event_type = 'spot_found' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar)) end) as double) - count(distinct case when event_type = 'ad_request' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar)) end)/cast(count(distinct case when event_type = 'spot_found' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar)) end) as double) as difference
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and event_type in('spot_found','ad_request')
      and site_key = 'site' 
      and middy_version in('old_version','new_version')
group by url_decode(url)
having count(distinct case when event_type = 'spot_found' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar)) end) >= 50 /*minimun 50 requests per URL, change it to whatever fits your data needs*/
order by 5 desc /*ordered by new spots descending*/

/*insert year,month,day,site,old and new versions - please note that you need to change the versions in the select as well as the where*/
