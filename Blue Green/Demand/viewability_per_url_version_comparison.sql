select url_decode(url) as url,
       count(distinct case when event_type = 'ad_impression' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as old_impressions,
       count(distinct case when event_type = 'viewed_impression' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as old_viewed_impressions,
       count(distinct case when event_type = 'viewed_impression' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'ad_impression' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) as old_viewability,
       count(distinct case when event_type = 'ad_impression' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as new_impressions,
       count(distinct case when event_type = 'viewed_impression' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as new_viewed_impressions,
       count(distinct case when event_type = 'viewed_impression' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'ad_impression' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) as new_viewability,
         count(distinct case when event_type = 'viewed_impression' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'ad_impression' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) - count(distinct case when event_type = 'viewed_impression' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end)/cast(count(distinct case when event_type = 'ad_impression' and middy_version = 'old_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) as double) as difference
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and event_type in('ad_impression','viewed_impression')
      and site_key = 'site' 
      and middy_version in('old_version','new_version')
      /* optional
      and demand_index = 0
      and attempt_index = 1*/
group by url_decode(url)
having count(distinct case when event_type = 'ad_impression' and middy_version = 'new_version' then concat(page_view_id, cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar)) end) >= 50 /*minimun 50 impressions per URL, change it to whatever fits your data needs*/
order by 5 desc /*ordered by new impressions descending*/

/*insert year,month,day,site,old and new versions - please note that you need to change the versions in the select as well as the where*/
