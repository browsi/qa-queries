select url,
       ad_index,
       sum(case when middy_version = 'old_version' then (fold*impressions) end)/cast(sum(case when middy_version = 'old_version' then impressions end) as double) as old_avg_fold,
       sum(case when middy_version = 'new_version' then (fold*impressions) end)/cast(sum(case when middy_version = 'new_version' then impressions end) as double) as new_avg_fold,
       sum(case when middy_version = 'new_version' then (fold*impressions) end)/cast(sum(case when middy_version = 'new_version' then impressions end) as double)-sum(case when middy_version = 'old_version' then (fold*impressions) end)/cast(sum(case when middy_version = 'old_version' then impressions end) as double) as difference,
       sum(case when middy_version = 'old_version' then impressions end) as old_impressions,
       sum(case when middy_version = 'new_version' then impressions end) as new_impressions       
from
(
select middy_version,
       url_decode(url) as url,
       ad_index,
       round(cast(fold as double),1) as fold,
       count(distinct page_view_id) as impressions
from demand
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and event_type = 'ad_impression'
      and site_key = 'site' 
      and middy_version in('old_version','new_version')
group by middy_version,
         url_decode(url),
         ad_index,
         round(cast(fold as double),1)
)
group by url,
         ad_index
order by 7 desc
