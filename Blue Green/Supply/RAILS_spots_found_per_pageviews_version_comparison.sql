select url,
       count (distinct case when middy_version = 'old_version' then page_view_id end) as old_pageviews,
       sum (case when middy_version = 'old_version' then spots_found end) as old_spots,
       sum (case when middy_version = 'old_version' then spots_found end)/cast(count (distinct case when middy_version = 'old_version' then page_view_id end) as double) as old_spots_per_pv,
       count (distinct case when middy_version = 'new_version' then page_view_id end) as new_pageviews,
       sum (case when middy_version = 'new_version' then spots_found end) as new_spots,
       sum (case when middy_version = 'new_version' then spots_found end)/cast(count (distinct case when middy_version = 'new_version' then page_view_id end) as double) as new_spots_per_pv,
       sum (case when middy_version = 'new_version' then spots_found end)/cast(count (distinct case when middy_version = 'new_version' then page_view_id end) as double)-sum(case when middy_version = 'old_version' then spots_found end)/cast(count (distinct case when middy_version = 'old_version' then page_view_id end) as double) as difference
from
(
select distinct
       url_decode(url) as url,
       middy_version,
       page_view_id,
       case when (left_rail_spots_found+right_rail_spots_found) > rails_max_items then rails_max_items else (left_rail_spots_found+right_rail_spots_found) end as spots_found /*potential spots filtered out*/
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type = 'page_view'
      and device_type = 'DESKTOP'
      and site_key = 'site'
      and middy_version in('old_version','new_version')
      and is_benchmark_pageview = false
)
group by url
order by 5 desc /*ordered by new_pageviews descending*/

