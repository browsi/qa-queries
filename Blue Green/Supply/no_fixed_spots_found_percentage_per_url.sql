select url_decode(url) as url,
       count (distinct case when fixed_spots_found = 0 then page_view_id end)/cast(count(distinct page_view_id) as double) as no_fixed_spots_percentage,
       count (distinct page_view_id) as total_pageviews
from supply
where est_time = date('date')
      and site_key = 'site'
      and event_type = 'page_view'
      and device_type <> 'DESKTOP' /*change to = 'DESKTOP' for desktop data*/
group by url_decode(url)
having count (distinct page_view_id) >= 500 /*number of minimum pageviews*/
order by 2 desc
