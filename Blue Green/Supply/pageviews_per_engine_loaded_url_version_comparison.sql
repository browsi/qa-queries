select url_decode(url) as url,
       count (distinct case when middy_version = 'old_version' and event_type = 'engine_loaded' then page_view_id end) as old_engine_loaded,
       count (distinct case when middy_version = 'new_version' and event_type = 'engine_loaded' then page_view_id end) as new_engine_loaded,
       count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end) as old_page_view,
       count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end) as new_page_view,
       count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' and event_type = 'engine_loaded' then page_view_id end) as double) as old_percentage,
       count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' and event_type = 'engine_loaded' then page_view_id end) as double) as new_percentage,
       count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' and event_type = 'engine_loaded' then page_view_id end) as double)-count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' and event_type = 'engine_loaded' then page_view_id end) as double) as difference
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type in('engine_loaded','page_view')
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
      and middy_version in('old_version','new_version')
group by url_decode(url)
order by 2 desc
