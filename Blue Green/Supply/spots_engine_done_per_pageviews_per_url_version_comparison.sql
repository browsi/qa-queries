select url_decode(url) as url,
       count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end) as old_pageviews,
       count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end) as new_pageviews,
       count (distinct case when middy_version = 'old_version' and event_type = 'spot_engine_done' then page_view_id end) as old_spot_engine_done,
       count (distinct case when middy_version = 'new_version' and event_type = 'spot_engine_done' then page_view_id end) as new_spot_engine_done,
       count (distinct case when middy_version = 'old_version' and event_type = 'spot_engine_done' then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end) as double) as old_percentage,
       count (distinct case when middy_version = 'new_version' and event_type = 'spot_engine_done' then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end) as double) as new_percentage,
       count (distinct case when middy_version = 'new_version' and event_type = 'spot_engine_done' then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end) as double)-count (distinct case when middy_version = 'old_version' and event_type = 'spot_engine_done' then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end) as double) as difference
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type in('page_view','spot_engine_done')
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
      and middy_version in('old_version','new_version')
group by url_decode(url)
order by 2 desc

