select url,
       count (distinct case when middy_version = 'old_version' then page_view_id end) as old_pageviews,
       count (distinct case when middy_version = 'old_version' and is_eligible = true then page_view_id end) as old_eligible_pageviews,
       count (distinct case when middy_version = 'old_version' and is_eligible = true then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' then page_view_id end) as double) as old_eligible_percentage,
       count (distinct case when middy_version = 'new_version' then page_view_id end) as new_pageviews,
       count (distinct case when middy_version = 'new_version' and is_eligible = true then page_view_id end) as new_eligible_pageviews,
       count (distinct case when middy_version = 'new_version' and is_eligible = true then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' then page_view_id end) as double) as new_eligible_percentage,
       count (distinct case when middy_version = 'new_version' and is_eligible = true then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' then page_view_id end) as double)-count (distinct case when middy_version = 'old_version' and is_eligible = true then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' then page_view_id end) as double) as difference
from
(
select url_decode(url) as url,
       middy_version,
       page_view_id,
       is_eligible
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type = 'page_view'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
      and middy_version in('old_version','new_version')
      and is_benchmark_pageview = false
)
group by url
order by 5 desc /*ordered by new_pageviews descending*/
