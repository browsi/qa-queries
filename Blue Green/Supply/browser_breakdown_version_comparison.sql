select a.site_key,
       a.browser,
       old_pageviews,
       new_pageviews,
       total_old_pageviews,
       total_new_pageviews,
       old_pageviews/cast(total_old_pageviews as double) as old_percentage,
       new_pageviews/cast(total_new_pageviews as double) as new_percentage
from
(
select site_key,
       browser,
       count (distinct case when middy_version = 'old_version' then page_view_id end) as old_pageviews,
       count (distinct case when middy_version = 'new_version' then page_view_id end) as new_pageviews
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type = 'page_view'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
      and middy_version in('old_version','new_version')
group by site_key,
         browser
)a

join

(
select site_key,
       count (distinct case when middy_version = 'old_version' then page_view_id end) as total_old_pageviews,
       count (distinct case when middy_version = 'new_version' then page_view_id end) as total_new_pageviews
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type = 'page_view'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
      and middy_version in('old_version','new_version')
group by site_key
)b

on a.site_key = b.site_key


