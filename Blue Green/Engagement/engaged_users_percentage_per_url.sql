select url_decode(url) as url,
       count (distinct case when middy_version = 'old_version' and event_type = 'user_engaged' then page_view_id end) as old_user_engaged,
       count (distinct case when middy_version = 'new_version' and event_type = 'user_engaged' then page_view_id end) as new_user_engaged,
       count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end) as old_pvs,
       count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end) as new_pvs,
       count (distinct case when middy_version = 'old_version' and event_type = 'user_engaged' then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end) as double) as old_engaged_percentage,
       count (distinct case when middy_version = 'new_version' and event_type = 'user_engaged' then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end) as double) as new_engaged_percentage,
       count (distinct case when middy_version = 'new_version' and event_type = 'user_engaged' then page_view_id end)/cast(count (distinct case when middy_version = 'new_version' and event_type = 'page_view' then page_view_id end) as double)-count (distinct case when middy_version = 'old_version' and event_type = 'user_engaged' then page_view_id end)/cast(count (distinct case when middy_version = 'old_version' and event_type = 'page_view' then page_view_id end) as double) as difference
from supply
where ((est_time = date_add('day',-1,current_date) and hour >= 11)
      or (est_time = current_date and hour between 0 and 4))
      and event_type in('page_view','user_engaged')
      and device_type in('MOBILE','TABLET') /*you can also change to DESKTOP*/
      and site_key = 'site'
      and middy_version in('old_version','new_version')
group by url_decode(url)
order by 3 desc /*ordered by new user engaged descending*/
