select url_decode(url) as url,
       count (distinct page_view_id) as pageviews,
       count (distinct case when is_eligible = true then page_view_id end) as eligible_pvs,
       count (distinct case when is_eligible = true then page_view_id end)/cast(count (distinct page_view_id) as double) as eligible_percentage
from supply
where est_time = date('yyyy-mm-dd')
      and event_type = 'page_view'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
      and is_benchmark_pageview = false
group by url_decode(url)
order by 2 desc /*order by pvs*/
