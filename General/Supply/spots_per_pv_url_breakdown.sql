select url,
       count (distinct page_view_id) as pageviews,
       sum (spots_found) as spots,
       sum (spots_found)/cast(count (distinct page_view_id) as double) as spots_per_pv
       
from
(
select distinct
       url_decode(url) as url,
       middy_version,
       page_view_id,
       case when spots_found > max_items then max_items else spots_found end as spots_found /*potential spots filtered out*/
from supply
where est_time = date('yyyy-mm-dd')
      and event_type = 'page_view'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
      and is_benchmark_pageview = false
)
group by url
order by 2 desc /*order by pvs*/
