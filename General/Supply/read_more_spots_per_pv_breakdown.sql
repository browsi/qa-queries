select a.url,
       count(distinct a.page_view_id) as pageviews,
       sum(case when spots_found > max_items then max_items else spots_found end) as spots_found,
       sum(case when spots_found > max_items then max_items else spots_found end)/cast(count(distinct a.page_view_id) as double) as spots_per_pv
from
(
select url_decode(url) as url,
       page_view_id,
       max_items
from supply
where est_time = date('2020-05-18')
      and event_type = 'page_view'
      and site_key = 'cnn'
      and device_type in ('MOBILE','TABLET')
)a

left join

(
select url_decode(url) as url,
       page_view_id,
       count(distinct concat(page_view_id,cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar))) as spots_found
from demand
where est_time = date('2020-05-18')
      and event_type = 'spot_found'
      and site_key = 'cnn'
      and device_type in ('MOBILE','TABLET')
      and spot_type like '%dynamic%'

group by 1,2
)b

on a.url = b.url
   and a.page_view_id = b.page_view_id

group by 1
order by 2 desc

