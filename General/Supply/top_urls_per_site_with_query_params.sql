select concat(url_decode(url),coalesce(query_params,'')) as url, 
       count (distinct page_view_id) as pageviews /*based on supply_config_sent event*/
from supply
where est_time = date('date')
      and site_key = 'site'
      and event_type = 'supply_config_sent'
group by concat(url_decode(url),coalesce(query_params,''))
order by 2 desc
