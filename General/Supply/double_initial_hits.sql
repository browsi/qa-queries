select count (distinct bootstrap_page_view_id) as total_bpvids,
       count (distinct case when appearances > 1 then bootstrap_page_view_id end) as bpvids_with_more_than_one_appearance,
       count (distinct case when appearances > 1 then bootstrap_page_view_id end)/cast(count (distinct bootstrap_page_view_id) as double) as percentage
from
(
select bootstrap_page_view_id,
       count (*) as appearances
from supply
where est_time = date('date')
      and event_type = 'initial_hit'
      and site_key = 'site'
group by bootstrap_page_view_id
)
