select a.url,
       pageviews,
       requests,
       requests/cast(pageviews as double) as requests_per_pv
from
(
select url_decode(url) as url,
       count(distinct page_view_id) as pageviews
from supply
where est_time = date('date')
      and event_type = 'page_view'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
group by 1
)a

left join

(
select url_decode(url) as url,
       count(distinct concat(page_view_id,cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar))) as requests
from demand
where est_time = date('date')
      and event_type = 'ad_request'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site'
group by 1
)b

on a.url = b.url

/*where requests/cast(pageviews as double) < 1*/ /*you can add a maximum requests per pv here*/

order by 2 desc
