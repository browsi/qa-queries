select site_key,
       count (distinct user_id) as users,
       count (distinct case when is_returning_user = true then user_id end) as returning_users,
       count (distinct case when is_returning_user = true then user_id end)/cast(count (distinct user_id) as double) returning_user_percentage
from supply
where est_time = date('date')
      and event_type = 'supply_config_sent'
      and device_type in('MOBILE','TABLET')
      and site_key = 'site' /*remove this line if you want to see all sites*/
group by site_key
order by count (distinct user_id) desc
