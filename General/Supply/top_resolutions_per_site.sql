select resolution,
       count (distinct page_view_id) as pvs
from supply
where est_time = date('date')
      and event_type = 'page_view'
      and site_key = 'site'
      and device_type = 'DESKTOP'
group by resolution
order by 2 desc

