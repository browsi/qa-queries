select site_key,
       ad_index,
       attempt_index,
       count (distinct concat(page_view_id,cast(ad_index as varchar),cast(coalesce(refresh_count,0) as varchar))) as requests
from demand
where est_time = date('date')
      and site_key = 'site'
      and device_type in('MOBILE','TABLET')
      and event_type = 'ad_request'
group by site_key,
         ad_index,
         attempt_index
order by 1,2,3
