select url_decode(url) as url,
       /*ad_index,*/
       count (distinct case when event_type = 'ad_impression' then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0) as varchar)) end) as impressions,
      count (distinct case when event_type = 'viewed_impression' then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0) as varchar)) end) as viewed_impressions,
      count (distinct case when event_type = 'viewed_impression' then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0) as varchar)) end)/cast(count (distinct case when event_type = 'ad_impression' then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0) as varchar)) end) as double) as viewability
from demand
where est_time = date('') /*insert date*/
      and site_key = 'site'
      and device_type in('MOBILE','TABLET')
      and event_type in('ad_impression','viewed_impression')
group by url_decode(url)/*,
         ad_index*/
order by count (distinct case when event_type = 'ad_impression' then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0) as varchar)) end) desc /*ordered by impression count descending*/
