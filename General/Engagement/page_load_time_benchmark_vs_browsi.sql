select url,
       count (distinct page_view_id) as pvs,
       coalesce(exp(avg(ln(case when traffic_type = 'benchmark' then page_load_time end))),0) as avg_page_load_time_benchmarked,
       coalesce(exp(avg(ln(case when traffic_type = 'monetized' then page_load_time end))),0) as avg_page_load_time_browsi
       
from 
(
select url_decode(url) as url,
       case when lower(cast(is_benchmark_pageview as varchar))='true' then 'benchmark' else 'monetized' end as traffic_type,
       page_view_id,
	   max(page_load_time) as page_load_time
from engagement
where est_time>=date('date')
      and site_key = 'site'
      and device_type <> 'DESKTOP' and event_type='general_engagement' 
      and time_on_page> 0
      and depth_percentage>0 and depth_percentage<=100 
      and page_load_time>0 and page_load_time<120000 
      and ltrim(geo_location)<>'' and ltrim(traffic_source)<>'' 
      and ltrim(publisher_key)<>'' and ltrim(site_key)<>'' 
      and traffic_source is not NULL 
      and try_cast(screen_length as integer) > 0 and try_cast(screen_length as integer) <= 1024
group by url_decode(url),
         case when lower(cast(is_benchmark_pageview as varchar))='true' then 'benchmark' else 'monetized' end, 
         page_view_id 
)
group by url
order by 2 desc
